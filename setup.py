import os
from setuptools import setup
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "dictinplace",
    version = "1.0",
    author = "Robert Izzard",
    author_email="rob.izzard@gmail.com",
    description = ("A package to alter Python dictionaries in place using as little memory as possible."),
    packages = ['dictinplace'],
    long_description = read('README.md'),
    classifiers = [
        "Programming Lanuage :: Python :: 3",
        "Licence :: OSI Approved :: GPL3",
        "Operating System :: OS Indepdnent",
    ],
    url = "https://gitlab.com/robizzard/dictinplace",
)
