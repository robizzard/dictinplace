#!/usr/bin/env python3

"""
Functions to do things recursively, in place, on dicts while using
as little RAM as possible.
"""

import dictinplace
from dictinplace import dictinplace,dictget,dictpop,dictdel,dictcount,no_value,no_key

import bigjson
import copy
import cysimdjson
from datasets import load_dataset
import dpath
from fastnumbers import float, try_float, INPUT, RAISE
import operator
import ijson
import inspect
import json
import json_stream
from json_stream.dump import JSONStreamEncoder
import orjson
import simplejson
import msgspec
import naya
import nujson
import os
import pathlib
import rich
import simdjson
import sys
import time
import ujson
from yapic import json as yapicjson

filenames = {
    'load' : 'test.json',
    'save' : '/tmp/test.write.json'
    }

load_ftypes = {
    'json' : 'rb',
    'orjson' : 'rb',
    'naya' : 'r',
    'simplejson' : 'rb',
    'simdjson' : 'rb',
    'ijson' : 'rb',
    'ujson' : 'rb',
    'nujson' : 'rb',
    'json_stream' : 'r',
    'msgspec' : 'r',
    #'bigjson' : 'rb', # fails
    #'datasets' : 'r',
    'cysimdjson' : 'rb',
    'yapicjson' : 'rb'
    }

save_ftypes = {
    'json' : 'w',
    'jsonfwrite' : 'w',
    'orjson' : 'wb',
    'simplejson' : 'w',
    'simdjson' : 'w',
    'ujson' : 'w',
    'nujson' : 'w',
    'json_stream' : 'w',
    }

memfile = '/tmp/dict_mem'
mmaxfile = '/tmp/dict_max'
reset_mmax_file = '/tmp/reset_mmax'

def reset_mmax():
    pathlib.Path(reset_mmax_file).touch()

# memory profiling using memory_profile??
#from memory_profiler import profile
# or dummy "profiler" decorator
def profile(func): return func

def memory_use(pid='self'):
    # return memory usage in MB
    with open(f'/proc/{pid}/status') as f:
        try:
            memusage = f.read().split('VmRSS:')[1].split('\n')[0][:-3]
        except:
            return 0
    return int(memusage.strip())/(1000)

def report_memory_use(pid='self',prev=None,mmax=None):
    # report memory use and (optionally) max memory use
    # if prev is given, only report if memory use changes
    x = memory_use(pid=pid)
    if prev == None or prev != x:
        if mmax == None:
            print(f"Memory {x} MB")
        else:
            print(f"Memory {x} MB : max {max(x,mmax)} MB")
    return x

def _testdict():
    return {
        "hello" : "world",
        "1234" : 1234.0,
        7890.0 : 7890,
        "nested dict": {
            "nested string key" : "nested value",
            "2468" : 2468,
            1357.0 : 1357,
            "nested dict in nested dict" : {
                "goodbye" : "cruel world"
            }
        },
        "dull" : "london",
        "array" : "Why don't you just make ten louder and make ten be the top number and make that a little louder?".split(' ')
    }

############################################################
############################################################
############################################################



def dictadd(d1,d2):
    """
    Add values in dict d2 to values in d1, return the added dict.
    Note: destroys both d1 and d2.
    """
    return dictinplace(
        d2,
        newdict = d1,
        funcs = {'value' : lambda value_in_d2, oldkeylist=None, **_:
                 value_in_d2 if (x := dictget(d1,oldkeylist,default=no_key)) is no_key \
                 else (x + value_in_d2)}
    )

def tofloat(x, **_):
    """
    Try to convert input, x, to a float, and return it.
    """
    try:
        x = float(x)
    except:
        pass
    return x

def tostring(x, **_):
    """
    Try to convert input, x, to a string, and return it.
    """
    try:
        x = str(x)
    except:
        pass
    return x


def save(d,method='json'):
    indent=4

    ensure_ascii=False
    sort_keys=True
    with open(filenames['save'],save_ftypes[method]) as f:
        if method == 'json':
            json.dump(d,f,indent=indent,ensure_ascii=ensure_ascii,sort_keys=sort_keys) # 0%, 67s
        if method == 'jsonfwrite':
            f.write(json.dumps(d,indent=indent,ensure_ascii=ensure_ascii,sort_keys=sort_keys)) # 503%, 73s
        elif method == 'orjson':
            f.write(orjson.dumps(d)) # 17.65%, 9.65s
        elif method == 'simdjson':
            simdjson.dump(d,f) # 0%, 68s
        elif method == 'simplejson':
            simplejson.dump(d,f) # 21%, 24s
        elif method == 'ujson':
            ujson.dump(d,f) # 38%, 12s
        elif method == 'json_stream':
            json.dump(d,f, cls=JSONStreamEncoder) # 0%, 70s
        elif method == 'cysimdjson':
            cysimdjson.dump(d,f) # 0%, 68s
        elif method == 'yapicjson':
            yapic.json.dump(d,f)
    return



@profile
def load(method = 'orjson'):
    with open(filenames['load'],load_ftypes[method]) as f:
        if method == 'json':
            d = json.load(f) # 15.8GB, 16s
        elif method == 'orjson':
            d = orjson.loads(f.read()) # 8.1GB, 7.7s
        elif method == 'simdjson':
            d = simdjson.load(f) # 10.0GB, 11.4s
        elif method == 'simplejson':
            d = simplejson.load(f) # 15.8GB, 15.23s
        elif method == 'ijson':
            d = dict(ijson.items(f,'')) # fails
        elif method == 'ujson':
            d = ujson.load(f) # 6.6GB, 15s
        elif method == 'nujson':
            d = nujson.load(f) #
        elif method == 'naya':
            d = naya.parse(f)
        elif method == 'json_stream': # 7.5GB, 119s, fails
            d = dict(json_stream.load(f,persistent=True))
        elif method == 'msgspec': # 16GB, 12s
            d = msgspec.json.decode(f.read())
        elif method == 'bigjson': # fails
            d = bigjson.load(f)
        elif method == 'datasets': # 15.9GB, 15s, error!
            d = dict(load_dataset("json",data_files=filenames['load']))
        elif method == 'cysimdjson':
            parser = cysimdjson.JSONParser()
            d = dict(parser.load(filenames['load']))
        elif method == 'yapicjson':
            d = yapicjson.loads(f.read())

        n = dictcount(d)
    return d



@profile
def process(d,vb=False):

    # do nothing : 0% (8s)
    #x = dictinplace(d,vb=vb)

    # change keys to floats : 0% (11s)
    #x = dictinplace(d,vb=vb,funcs={'key':tofloat})

    # change values to float : 0.11% (28s)
    #x = dictinplace(d,vb=vb,funcs={'value':tofloat})

    # changes keys and values to floats : 21% (64s)
    #x = dictinplace(d,vb=vb,funcs={'key':tofloat,'value':tofloat})

    # change keys and values and keylists to floats : 21% (88s)
    x = dictinplace(d,vb=vb,funcs={'key':tofloat,'value':tofloat})

    return x

def current_mem():
    # get current memory use from disk as a float
    while os.path.exists(memfile) == False:
        time.sleep(0.1)
    with open(memfile,"r") as f:
        return(f.read().strip())
    return 0.0

def current_mmax():
    # get current memory use from disk as a float
    while os.path.exists(mmaxfile) == False:
        time.sleep(0.1)
    with open(mmaxfile,"r") as f:
        return float(f.read().strip())
    return 0.0

def timetest(header,
             code,
             nsleep=5,
             vb=False,
             data=None):
    """
    Timing test of the code in "code". The header is used for logging.

    data contains the pre-loaded dict. This is deleted after use.
    """
    reset_mmax()
    time.sleep(nsleep)
    mem0 = current_mmax()
    t0 = time.time()
    if isinstance(data,dict):
        nkeys = dictcount(data)
    else:
        nkeys = 0
    if vb:
        print(f"mem0 = {mem0}, t0 = {t0}, data? {data==True}, nkeys {nkeys}")
    if data is None:
        data = {}

    # to exec the code we need to set up the namespace
    # and provide access to the data dict
    namespace = globals().copy()
    namespace['data'] = data
    exec(code,namespace) # slow part
    # and copy the data back from the exec's local namespace
    # into the data dict
    data = namespace['data']

    t1 = time.time()
    time.sleep(nsleep)
    mem1 = current_mmax()
    if vb:
        print(f"mem1 = {mem1}, t1 = {t1}, data? {data==True}, nkeys {dictcount(data)}")
    dt = t1 - t0
    dmem = mem1 - mem0
    if data is not None and data != False:
        nkeys = dictcount(data)
    else:
        nkeys = 0
    print(f"{header} {dmem:g} {dmem/mem0*100.0:5.2f} % {dt:5.2f} s {nkeys}")
    del data
    time.sleep(nsleep)
    reset_mmax()
    if vb:
        print(f"postcleanup mem {mem1}")
    return (dmem,dt)

def dotests(loadtests=False,
            savetests=False,
            dicttests=False,
            addtest=True):

    if loadtests:
        # JSON load tests
        for method in load_ftypes.keys():
            header = f'Load {method}'
            code = f"data = load(method='{method}')"
            timetest(header,code)

        sys.exit()

    if savetests:
        # JSON save tests
        data = load()
        for method in save_ftypes.keys():
            header = f'Save {method}'
            code = f"save(data,method='{method}')"
            timetest(header,code,data=data)
        del data

    if dicttests:
        # test dictinplace
        header = f'dictinplace keys '
        code = "data = dictinplace(data,funcs={'key':tofloat})"
        timetest(header,code,data=load(),vb=False)

        header = f'dictinplace values '
        code = "data = dictinplace(data,funcs={'value':tofloat})"
        timetest(header,code,data=load(),vb=False)

        header = f'dictinplace keys_then_values '
        code = "data = dictinplace(data,funcs={'key':tofloat});data = dictinplace(data,funcs={'value':tofloat})"
        timetest(header,code,data=load(),vb=False)

        header = f'dictinplace keys+values '
        code = "data = dictinplace(data,funcs={'key':tofloat,'value':tofloat})"
        timetest(header,code,data=load(),vb=False)

    if addtest:
        # test dictionary addition (on small dicts)
        d1 = _testdict()
        d2 = _testdict()
        rich.print(f"Add d1 {d1} \n...and d2 {d2}\n")
        dictadd(d1,d2)
        rich.print(f"Result\n{d1}")


# Fork to measure timing in separate task
if os.fork() > 0:
    # run tests
    dotests(loadtests=False,
            savetests=False,
            dicttests=False,
            addtest=False)
else:
    # child does the logging
    ppid = os.getppid()
    prev = None
    mmax = None
    pre_reset = None
    post_reset = None
    sleeptime = 0.1 # seconds
    while True:
        # exit on parent death
        try:
            os.kill(ppid,0)
        except OSError:
            sys.exit()

        if False:
            prev = report_memory_use(pid=ppid,
                                     prev=prev,
                                     mmax=mmax)
        else:
            prev = memory_use(pid=ppid)

        # write to disk
        with open(memfile,'w') as f:
            if prev is None:
                f.write("0.0\n")
            else:
                f.write(f"{prev}\n")

        if os.path.isfile(reset_mmax_file):
            pre_reset = mmax
            mmax = None
            os.remove(reset_mmax_file)
            post_reset = memory_use(pid=ppid)
            with open(mmaxfile,'w') as f:
                f.write(f"{prev}\n")

        if mmax == None or prev > mmax:
            mmax = prev
        with open(mmaxfile,'w') as f:
            if mmax is None:
                f.write("0.0\n")
            else:
                f.write(f"{mmax}\n")
        time.sleep(sleeptime)
