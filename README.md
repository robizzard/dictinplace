# dictinplace

dictinplace : conveneince routines to modify dictionaries
              (almost) in place.

Copyright 2025, Robert Izzard.

Thanks to Jason R. Coombs for the mutable_iter function
(see their LICENCE below).

Use at your own risk! Please see the LICENCE file for
licencing details.


Usage:

```
import dictinplace
from dictinplace import dictinplace

dictinplace(dict1, 
            dict2=None, 
            keylist=[],
            funcs=None, # dict of funcs
            data=None,
            autocount=False,
            vb=False)
```

This is the main function of dictinplace. It loops through `dict1`,
applies the given `funcs` to each `key,value` pair of `dict1`, and fills
`dict2` with the resulting `newkey,newvalue`. At the same time it empties
`dict1`. Thus you should expect `dict1` to be destroyed by `dictinplace()`.
The assocaited memory overheads are thus, hopefully, small. 

`dict2` can be `None` if you just want a modified version of `dict1`.

`dict2` can have pre-existing content, in which case your function should understand how to deal with the content (best to see the examples below).

If you want to, say, add two dicts, `dict1` should be the smaller and `dict2` should be larger. This is because we loop over `dict1` to perform the task. You can set `autocount=True` to automatically compute which is larger, and hence optimize automatically, but bear in mind there is some overhead associated with the counting, so if you know already which is larger, set this in `dict2`.

`dict2` can be empty, in which case `dict1` is processed, and destroyed, and the result is put in `dict2`.

The location in each dict is set by a `keylist` which is a Python list (`[...]`) of keys.

The `keylist` argument refers to the location of `dict2` values relative to a parent location. This is mostly useful for internal recursion, but also enables you to manually shift the location of data inside a dict.

You can set `data` to whatever you like: this is just an object that is passed to each function declared in the `funcs` dict.

By setting the `funcs` dict you can choose a function to apply to either
keys (via `key`) or values (via `value`), or both.

```
funcs = {
    "key" : keyfunc,
    "value" : valuefunc
}
```

Don't set a function if you don't want to use it!

The functions are applied in the order you set them in the `dict` according to Python: thus if you are using an old Python (<3.6) that does not respect `dict` ordering, you should use an `OrderedDict`.


The functions should be declared as follows:

```
    func(scalar,
         funcs=funcs,
         data=data,
         dict1=dict1,
         dict2=dict2,
         oldkeylist=oldkeylist,
         newkeylist=newkeylist)
```

The scalar is the (new) key, if `"key"`, and the value if `"value"`.

The `oldkeylist` is the location of the value in the `dict1`.
The `newkeylist` is the location of the value in the `dict2`, which may well be differet to in `dict1`.

You can then obtain the key from `oldkeylist[-1]` or `newkeylist[-1]`,
depending on which you require, and you can access your input value using
`dict1[oldkeylist[-1]]` and set it in `dict2[newkeylist[-1]]`.

Note that your functions probably require a dummy `**_` to absorb any unused
named variables, this way you do not have to declare all arguments. Most of
the examples below follow this convention.


Convenience functions
=====================

`dictinplace` provides a few functions to make your life a bit easier. Note
that these are similar to those found elsewhere (e.g. `benedict`) but included
here to allow for a codebase with fewer dependencies.

```
dictget(d,keylist,default=None)
```

Returns the value in dict `d` at location given by `keylist`. For example, if your keylist is such that `keylist=[1,"a",whatever]` then this function returns `d[1]["a"][whatever]`.

If a key,value pair does not exist, but you try to access it, returns `default` (if defined) otherwise raises a `KeyError`,


```
dictpop(d,keylist,default=None)
```
As `dictget` but `pop`s the value (i.e. deletes it and returns it).


```
dictdel(d,keylist)
```
Deletes the value in d at the given keylist.


```
dictset(d,keylist,value)
```

Sets the given `value` at a location in `d` given by `keylist`. Hence, `dictset(d,[1,'b',whatever],value)` is equiavlent to `d[1]['b'][whatever] = value`, except that nested locations are created as required.

```
dictcount(d,max=0)
```
Returns the total number of key,value pairs in dict `d`.

If `max` is non-zero, the count stops at `max`. This is useful when comparing two dicts to find out which is larger, as it saves excessive counting.

Examples
========

You could convert all your `dict1`'s keys to `float` types, when possible, with:

```
import dictinplace
from dictinplace import dictinplace

def tofloat(x, **_):
    try:
        x = float(x)
    except:
        pass
    return x

dict1 = { ... }
dict2 = dictinplace(oldict,funcs={'key':tofloat})
```

You can convert all values to strings with:

```
import dictinplace
from dictinplace import dictinplace

def tostring(x, **_):
    try:
        x = str(x)
    except:
        pass
    return x

dict1 = { ... }
dict2 = dictinplace(oldict,funcs={'value':tostring})
```

You can combine both the above into one call with

```
import dictinplace
from dictinplace import dictinplace

def tofloat(x, **_):
    try:
        x = float(x)
    except:
        pass
    return x
def tostring(x, **_):
    try:
        x = str(x)
    except:
        pass
    return x

dict2 = dictinplace(oldict,funcs={'key':tofloat,'value':tostring})
```

In your function you have access to the old key list, `oldkeylist`, the new (i.e. perhaps modified by your functions) key list `newkeylist`, data passed in through `data` (which you can be set to anything you like).


You can delete items by setting the newkey to, i.e. have your "key" function return, `dictinplace.no_key`.


You can add dict d2 to d1 with:

```
import dictinplace
from dictinplace import dictinplace, dictget, no_key

def dictadd(d1,d2):
    """
    Add values in dict d2 to values in d1, return the added dict.

    Note: This empties d2.
          The returned dict is the same dict as d1 (modified).

    Note: will fail for types for which "+" is undefined, e.g. bool.
    """
    return dictinplace(
        d2,
        dict2=d1,
        funcs={
            'value' : lambda value_in_d2, oldkeylist=None, **_:
            value_in_d2 if (x := dictget(d1,oldkeylist,default=no_key)) is no_key \
            else (x + value_in_d2)
        }
    )


d1 = {"shared int" : 10,
      "shared string" : "inside d1",
      "d1 only" : 20}
d2 = {"shared int" : 100,
      "shared string" : "inside d2",
      "d2 only" : 30,
      "stringkey" : "stringvalue"}

dict1 = { ... }
dict2 = { ... }
dictadd(dict1,dict2)
print(dict1)

# {'d1 only': 20, 'shared int': 110, 'shared string': 'inside d1inside d2', 'd2 only': 30, 'stringkey': 'stringvalue'}

```

You can do the same with a separate function.

```

def dictadd(d1,d2):
    """
    Add values in dict d2 to values in d1, return the added dict.
    Note: destroys both d1 and d2.
    """
    def _add(value_in_d2,
             dict2=None,
             oldkeylist=None,
             **_):
             
        if (x := dictpop(dict2,oldkeylist,default=no_key)) is no_key:
            # no key in dict2, just set
            return value_in_d2
        else:
            # add to key in dict2
            return x + value_in_d2

    return dictinplace(
        d2,
        dict2 = d1,
        funcs = {'value':_add}
    )
```

We use `dictget` to acquire the value in d2 corresponding to the location, `oldkeylist`, in d1. If the value is not found in d1, the default is to return no_key

Note: This example requires Python 3.8+ because we use the walrus operator (:=) to avoid two `dictget()` lookups.

## License
For open source projects, say how it is licensed.

## Project status
As of 2025, supported.
