"""


dictinplace Examples
====================

You could convert all your `olddict`'s keys to `float` types, when possible, with:

```
import dictinplace
from dictinplace import dictinplace

def tofloat(x, **_):
    try:
        x = float(x)
    except:
        pass
    return x

olddict = { ... }
newdict = dictinplace(oldict,funcs={'key':tofloat})
```

You can convert all values to strings with:

```
import dictinplace
from dictinplace import dictinplace

def tostring(x, **_):
    try:
        x = str(x)
    except:
        pass
    return x

olddict = { ... }
newdict = dictinplace(oldict,funcs={'value':tostring})
```

You can combine both the above into one call with

```
import dictinplace
from dictinplace import dictinplace

def tofloat(x, **_):
    try:
        x = float(x)
    except:
        pass
    return x
def tostring(x, **_):
    try:
        x = str(x)
    except:
        pass
    return x

newdict = dictinplace(oldict,funcs={'key':tofloat,'value':tostring})
```

In your function you have access to the old key list, `oldkeylist`, the new (i.e. perhaps modified by your functions) key list `newkeylist`, data passed in through `data` (which you can be set to anything you like).


You can delete items by setting the newkey to, i.e. have your "key" function return, `dictinplace.no_key`.


You can add dict d2 to d1 with:


```
import dictinplace
from dictinplace import dictinplace, dictget, no_key

def dictadd(d1,d2):
    \"\"\"
    Add values in dict d2 to values in d1, return the added dict.

    Note: This empties d2.
          The returned dict is the same dict as d1 (modified).

    Note: will fail for types for which "+" is undefined, e.g. bool.
    \"\"\"
    return dictinplace(
        d2,
        newdict=d1,
        funcs={
            'value' : lambda value_in_d2, oldkeylist=None, **_:
            value_in_d2 if (x := dictget(d1,oldkeylist,default=no_key)) is no_key \
            else (x + value_in_d2)
        }
    )


d1 = {"shared int" : 10,
      "shared string" : "inside d1",
      "d1 only" : 20}
d2 = {"shared int" : 100,
      "shared string" : "inside d2",
      "d2 only" : 30,
      "stringkey" : "stringvalue"}

dict1 = { ... }
dict2 = { ... }
dictadd(dict1,dict2)
print(dict1)

# {'d1 only': 20, 'shared int': 110, 'shared string': 'inside d1inside d2', 'd2 only': 30, 'stringkey': 'stringvalue'}

```

You can do the same with a separate function.

```

def dictadd(d1,d2):
    \"\"\"
    Add values in dict d2 to values in d1, return the added dict.
    Note: destroys both d1 and d2.
    \"\"\"
    def _add(value_in_d2,newdict=None,oldkeylist=None,**_):
        if (x := dictpop(newdict,oldkeylist,default=no_key)) is no_key:
            return value_in_d2
        else:
            return x + value_in_d2

    return dictinplace(
        d2,
        newdict = d1,
        funcs = {'value':_add}
    )
```



We use `dictget` to acquire the value in d2 corresponding to the location, `oldkeylist`, in d1. If the value is not found in d1, the default is to return no_key

Note: This example requires Python 3.8+ because we use the walrus operator (:=) to avoid two `dictget()` lookups.

"""
