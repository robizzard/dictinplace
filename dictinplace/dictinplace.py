"""
dictinplace : conveneince routines to modify dictionaries
              (almost) in place.

Copyright 2025, Robert Izzard.

Thanks to Jason R. Coombs for the mutable_iter function
(see their LICENCE below).

Use at your own risk! Please see the LICENCE file for
licencing details.


Usage:

```
import dictinplace
from dictinplace import dictinplace

dictinplace(dict1,
            dict2=None,
            keylist=[],
            funcs=None, # dict of funcs
            data=None,
            vb=False)
```

This is the main function of dictinplace. It loops through `dict1`,
applies the given `funcs` to each `key,value` pair of `dict1`, and fills
`dict2` with the resulting `newkey,newvalue`. At the same time it empties
`dict1`. Thus you should expect `dict1` to be destroyed by `dictinplace()`.
The assocaited memory overheads are thus, hopefully, small.

You usually want `dict2` and `keylist` to be unset: these are used internally
to recursively traverse the `dict1`.

The `keylist` refers to the location of dict2 value relative to some parent
dict, which is mostly useful for internal recursion.

You can set `data` to whatever you like: this is passed to each function declared in the `funcs` dict.

By setting the `funcs` dict you can set functions to apply to either
keys or a value. Funcs should be set with one or both of two keys:

```
funcs = {
    "key" : keyfunc,
    "value" : valuefunc
}
```
Don't set a function if you don't want to use it!

The functions are applied in the order you set them in the `dict` according to Python: thus if you are using an old Python (<3.6) that does not respect `dict` ordering, you should use an `OrderedDict`.


The functions are called with sufficient information to be able to do pretty much anything:

```
    func(scalar,
         funcs=funcs,
         data=data,
         dict1=dict1,
         dict2=dict2,
         oldkeylist=oldkeylist,
         newkeylist=newkeylist)
```

The scalar is the (new) key, if `"key"`, and the value if `"value"`.

The `oldkeylist` is the location of the value in the `dict1`.
The newkeylist is the new location in dict2, perhaps modified
by the funcs.

You can obtain the current key from oldkeylist[-1] or newkeylist[-1],
depending on which you require.

Note that your functions require a dummy `**_` to absorb any unused
named variables.


Convenience functions
=====================

dictinplace provides a few functions to make your life a bit easier. Note
that these are similar to those found elsewhere (e.g. benedict) but included
here to allow for a more compact codebase.

```
dictget(d,keylist,default=None)
````

Returns the value in dict `d` at location given by `keylist`. For example, if your keylist is such that `keylist=[1,"a",whatever]` then this function returns `d[1]["a"][whatever]`.

If a key,value pair does not exist, but you try to access it, returns `default` (if defined) otherwise raises a `KeyError`,


```
dictpop(d,keylist,default=None)
````
As `dictget` but `pop`s the value (i.e. deletes it, from the dict, and returns it).


```
dictdel(d,keylist)
````
Deletes the value in `d` at the given keylist without returning it.


```
dictset(d,keylist,value)
```

Sets the given `value` at a location in `d` given by `keylist`. Hence, `dictset(d,[1,'b',whatever],value)` is equiavlent to `d[1]['b'][whatever] = value`, except that nested locations are created as required.

```
dictcount(d)
```
Returns the total number of key,value pairs in dict `d`.

"""

__version__="1.0"
from collections.abc import Mapping


# sentinels
no_key = object()
no_value = object()
no_default = object()

def dictcount(d):
    """
    Count (recursively) the number of keys in dict d.
    """
    n = 0
    for v in d.values():
        n += (1+dictcount(v)) if isinstance(v,Mapping) else 1
    return n

def _mutable_iter_pop(dict):
    """
    Iterate over items in the dict, yielding the first one, but allowing
    it to be mutated during the process.

    The items are popped off the dict, emptying it in the process.

    Based on mutable_iter from jaraco.itertools
    https://github.com/jaraco/jaraco.itertools/blob/be08cb5d04f9434e140cf7f26483c261b2cff41c/jaraco/itertools.py#L1152-L1168

    Licence for this function:

Copyright Jason R. Coombs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    """
    while dict:
        prev_key = next(iter(dict))
        yield prev_key, dict.pop(prev_key)

def dictinplace(dict1,
                dict2=None,
                keylist=[],
                funcs=None,
                data=None,
                autocount=False,
                vb=False):
    """
    Convert dict1 to dict2, applying functions given in the funcs dict
    in the order they are specified in the dict.

    Returns the dict2.

    If no functions are set, just returns dict1
    (but does not copy its contents).
    """
    if vb:
        print(f"dictinplace at keylist {keylist}")

    # no funcs specified: just return the old dict
    if funcs == None:
        return dict1

    # create new dict if required
    if dict2 == None:
        dict2={}

    if autocount == True:
        # determine which is the largest dict, put that in dict1
        countold = dictcount(dict1)
        countnew = dictcount(dict2,countold)
        if countold < countnew:
            countold,countnew = countnew,countold

    # loop over keys and values in the dict1 using the
    # mutable iter
    for key,value in _mutable_iter_pop(dict1):
        oldkeylist = keylist + [key]
        newkey = key
        newkeylist = list(oldkeylist) # a copy!

        if vb:
            print(f"key: {key} (at {oldkeylist}) {type(key)}")

        # Now we want to apply given funcs to this key,value
        # pair.
        #
        # We require a value function, if one does
        # not exist set it to None.
        if not 'value' in funcs:
            funcs['value'] = None

        # loop in native dict order (on modern Python
        # this is in order that are set, one can always
        # use an OrderedDict in older Python)
        for functype,func in funcs.items():
            if functype == 'key':
                # apply key function to change the newkey
                newkey = func(newkey,
                              funcs=funcs,
                              data=data,
                              dict1=dict1,
                              dict2=dict2,
                              oldkeylist=oldkeylist,
                              newkeylist=newkeylist)
                if newkey == no_key:
                    break
                newkeylist = keylist + [newkey]
                if vb:
                    print(f"newkey (from key func): {newkey} {type(newkey)}")

            elif newkey != no_key and functype == 'value':
                # we now set the value but only if newkey
                # exists (it cannot be no_key)
                if func == None:
                    # we have no func, just set the value from the old value
                    # so its data are preserved
                    dict2[newkey] = value
                    if vb:
                        print(f"newvalue (==oldvalue): {dict2[newkey]} {type(dict2[newkey])}")
                else:
                    # we have a func to apply...
                    if vb:
                        print(f"newkeylist: {newkeylist}")
                        print(f"... value: {value} {type(value)}")

                    if isinstance(value,Mapping):
                        # nested dict (or similar): recurse
                        if vb:
                            print(f"process nested dict on value {value}")
                        if not newkey in dict2:
                            dict2[newkey] = {}

                        dict2[newkey] = dictinplace(value,
                                                      autocount=False,
                                                      dict2=dict2[newkey],
                                                      keylist=newkeylist,
                                                      funcs=funcs,
                                                      data=data,
                                                      vb=vb)
                    else:
                        # call the func to set the new value
                        dict2[newkey] = func(value,
                                             autocount=False,
                                             funcs=funcs,
                                             data=data,
                                             dict1=dict1,
                                             dict2=dict2,
                                             oldkeylist=oldkeylist,
                                             newkeylist=newkeylist)
                        if vb:
                            print(f"newvalue (from value func): {dict2[newkey]} {type(dict2[newkey])}")

    # return replacement dict
    return dict2


def dictget(d,keylist,default=no_default):
    """
    Return the value in dict d given by the keylist.
    """
    for key in keylist:
        if not key in d:
            if default is no_default:
                raise KeyError(f"Key \"{key}\" not found in key list {list(d.keys())}")
            else:
                return default
        d = d.get(key)
    return d

def dictpop(d,keylist,default=no_default,delete_empty_parents=False):
    """
    Pop and return the value in dict d given by the keylist.

    TODO: test delete_empty_parents
    """
    lastkey = keylist.pop()
    for key in keylist:
        if not key in d:
            if default is no_default:
                raise KeyError(f"Key \"{key}\" not found in key list {list(d.keys())}")
            else:
                return default
        d = d[key]

    x = d.pop(lastkey)
    if delete_empty_parents:
        # TODO
        if isinstance(d,Mapping) and d == False:
            del d

    return x

def dictdel(d,keylist):
    """
    Delete the value at the given keylist in d.

    TODO: implement delete_empty_parents
    """
    lastkey = keylist.pop()
    for key in keylist:
        if not key in d:
            if default is no_default:
                raise KeyError(f"Key \"{key}\" not found in key list {list(d.keys())}")
            else:
                return default
        d = d.get(key)
    del d[lastkey]

def dictset(d,keylist,value):
    """
    Given a dict d, and a key location given by keylist,
    set this location to the given value. Missing locations
    are constructed.

    Returns the value set.
    """
    lastkey = keylist.pop(-1)
    for key in keylist:
        if not key in d:
            d[key] = {}
        d = d[key]
    d[lastkey] = value
    return value
